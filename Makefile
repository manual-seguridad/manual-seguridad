NAME = seginf
XSLTPROC = xsltproc

IMAGES = images
STYLESHEET = seginf.css

XML_SRC = seginf.xml
XML_XSL = seginf.xsl
FO_XSL = /usr/share/xml/docbook/stylesheet/docbook-xsl/fo/docbook.xsl
#FO_XSL = /usr/share/xml/docbook/stylesheet/nwalsh/fo/docbook.xsl

CHUNKED_OUTPUT = chunked/
CHUNKED_XSLTPROC_OPTS = --stringparam base.dir $(CHUNKED_OUTPUT)

all: html

chunked:
	mkdir -p $(CHUNKED_OUTPUT)
	$(XSLTPROC) $(CHUNKED_XSLTPROC_OPTS) --nonet \
		$(XML_XSL) $(XML_SRC)
	cp -r images $(CHUNKED_OUTPUT)
	cp $(STYLESHEET) $(CHUNKED_OUTPUT)

clean:
	rm -fr $(CHUNKED_OUTPUT)

static:
	cp -r images $(CHUNKED_OUTPUT)
	cp $(STYLESHEET) $(CHUNKED_OUTPUT)

pdf:
	$(XSLTPROC) -o $(NAME).fo $(FO_XSL) $(XML_SRC)
	fop -fo $(NAME).fo -pdf $(NAME).pdf
