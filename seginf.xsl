<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:import href="http://docbook.sourceforge.net/release/xsl/current/xhtml/chunk.xsl"/>

<!-- in this we define how is the generated xhtml
     see for reference:
     http://docbook.sourceforge.net/release/xsl/current/doc/html/index.html -->

<xsl:param name="html.stylesheet" select="'./seginf.css'"/>

<xsl:param name="navig.graphics" select="1"/>
<xsl:param name="navig.graphics.extension" select="'.png'"/>
<xsl:param name="navig.graphics.path">./images/icons/</xsl:param>
<xsl:param name="navig.showtitles">0</xsl:param>

<xsl:param name="make.valid.html" select="1"></xsl:param>
<xsl:param name="html.cleanup" select="1"></xsl:param>

<xsl:param name="toc.section.depth">0</xsl:param>

</xsl:stylesheet>
